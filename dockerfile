FROM node:12.4.0-alpine as debug

WORKDIR /work2/

COPY ./src/package.json /work2/package.json
RUN npm install
RUN npm install -g nodemon

COPY ./src/ /work2/src/

ENTRYPOINT [ "nodemon","--inspect=0.0.0.0","./src/app.js" ]

FROM node:12.4.0-alpine as prod

WORKDIR /work2/
COPY ./src/package.json /work2/package.json
RUN npm install
COPY ./src/ /work2/

CMD node .